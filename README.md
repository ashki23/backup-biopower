# Input data and R scripts for manuscript "Impact of biopower generation on eastern US forests"

**REPOSITORY CITATION**

Mirzaee, Ashkan. Data and scripts for manuscript "Impact of biopower generation on eastern US forests" (2021). https://doi.org/10.6084/m9.figshare.14738277

**RELATED PUBLICATION**

Mirzaee, A., McGarvey, R.G., Aguilar, F.X. et al. Impact of biopower generation on eastern US forests. Environ Dev Sustain (2022). https://doi.org/10.1007/s10668-022-02235-4

## Access conditions
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.
Sourcecode is available under a [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Contents
All input data and R scripts to provide manuscript results and graphics. This is a backup repository of input data for [rc-biopower](https://gitlab.com/ashki23/rc-biopower).

## Contact information
- Ashkan Mirzaee: amirzaee@mail.missouri.edu

## System requirement
The workflow requires a Unix shell which is a Linux or macOS terminal or a [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) on Windows 10.

## Software
The following software is required:

- [Miniconda3](https://docs.conda.io/projects/conda/en/latest/user-guide/install/#regular-installation)

## Workflow
The workflow includes:

- Clone the repositoty
- Create a conda env for R and required packages
- Run the R script

For running the workflow, open a Unix shell (Linux or macOS terminal or [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) on Windows 10) and run:

```bash
git clone git@gitlab.com:ashki23/backup-biopower.git
cd backup-biopower
conda create --yes --prefix ./power_r_env --channel conda-forge --file ./power_r_env.txt
ln -s ${PWD}/power_r_env/lib/libproj.so ${PWD}/power_r_env/lib/libproj.so.15
conda activate ./power_r_env
Rscript statistical_analysis.R
```
---
<div align="center">
Copyright 2021-2022, [Ashkan Mirzaee](https://ashki23.github.io/index.html) | Content is available under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) | Sourcecode licensed under [GPL-3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
</div>
